package ai.wavelabs.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ai.wavelabs.demo.model.Student;

public interface StudentRepository extends JpaRepository<Student,Integer>
{

}
