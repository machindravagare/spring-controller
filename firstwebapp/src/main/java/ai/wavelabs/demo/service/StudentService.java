package ai.wavelabs.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ai.wavelabs.demo.model.Student;
import ai.wavelabs.demo.repository.StudentRepository;

@Service
public class StudentService
{
	@Autowired
	private StudentRepository studentRepository;
	

	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}
	public Student saveStudent(Student s)
	{
		return studentRepository.save(s);
	}

}
