package ai.wavelabs.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.wavelabs.demo.model.Student;
import ai.wavelabs.demo.service.StudentService;

@RestController
public class StudentController 
{
	@Autowired
	private StudentService studentService;
	
	@PostMapping(value="/students")
	public ResponseEntity<Student> saveStudent(@RequestBody Student students) {
		return ResponseEntity.status(201).body(studentService.saveStudent(students));
	}
	
	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Student>> getStudents()
	{
		List<Student> students = studentService.getAllStudents();
		return ResponseEntity.status(200).body(students);
	}


}
